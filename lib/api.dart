import 'package:redstone/redstone.dart';

@Group('/api')
class Api
{
  @Route('blah')
  String blah() => 'blah';
}