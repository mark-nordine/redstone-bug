import 'package:redstone/redstone.dart';
import '../lib/my_lib.dart'; // ignore: unused_import

main()
{
  showErrorPage = false;

  redstoneSetUp([#my_lib]);
  setupConsoleLog();
  start(port: 4002);
}